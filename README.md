# se-ref-java
This Java example uses gradle to build and run. Ensure you are using Gradle 4.6+.

## How to Run Example
1) Open src/main/java/App.java and add values for _API_KEY_ and _VENUE_ID_

2) Return back to the root directory and use the follow command to build and run example:
```
	gradle build
	gradle run
```
