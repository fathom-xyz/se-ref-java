import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.deepstream.*;
import io.deepstream.EventListener;
import io.deepstream.List;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class App {
    public static void main(String[] args) throws InvalidDeepstreamConfig {
        new SubscriberApplication();
    }

    static class SubscriberApplication {
        String API_KEY = "";
        String VENUE_ID = "";
        String TOPIC = "devices/positions";
        SubscriberApplication() throws InvalidDeepstreamConfig {
            try {
                DeepstreamClient client = new DeepstreamClient("wss://stream.fathomsys.com?apiKey=" + API_KEY);

                JsonObject credentials = new JsonObject();
                credentials.addProperty("apiKey", API_KEY);

                LoginResult loginResult = client.login(credentials);
                if (!loginResult.loggedIn()) {
                    System.err.println("Failed to login " + loginResult.getErrorEvent());
                } else {
                    System.out.println("Login Success");
                    JsonObject data = (JsonObject) loginResult.getData();
                    data.addProperty("venueId", VENUE_ID);
                    data.addProperty("topic", TOPIC);
                    subscribeEvent(client, data);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        private void subscribeEvent(DeepstreamClient client, JsonObject data) {
            client.event.subscribe("topic/" 
            + data.get("organizationId").getAsString() + "/"
            + data.get("venueId").getAsString() + "/"
            + data.get("topic").getAsString(), new EventListener() {
                @Override
                public void onEvent(String eventName, Object args) {
                    System.out.println(args);
                }
            });
        }
    }
}
